#include "bluepill.h"
#include "Spi.h"
#include "Pin.h"

#ifndef SDCARD_H_
#define SDCARD_H_

class SDCard {

	static Spi sd_spi;
	static Pin sd_cs;

public:
	SDCard(SPI_TypeDef* _spi = SPI1, uint8_t _cs = PA4);
	static void select(void);
	static void deselect(void);
	static void init_spi(SPI_TypeDef* _spi = SPI1, uint8_t _cs = PA4);
};

#endif /* SDCARD_H_ */
