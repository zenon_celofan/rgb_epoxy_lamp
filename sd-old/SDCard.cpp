/*
 * SDCard.cpp
 *
 *  Created on: 22 Jan 2020
 *      Author: n
 */

#include <SDCard.h>
#include "ff.h"
#include "diskio.h"

extern DSTATUS disk_initialize (BYTE drv);



SDCard::SDCard(SPI_TypeDef* _spi, uint8_t _cs) {
	sd_spi = _spi;
	_cs = PA4;

	disk_initialize(0);
} //SDCard()



void SDCard::select(void) {
	sd_cs.digital_write(Bit_RESET);
} //select()



void SDCard::deselect(void) {
	sd_cs.digital_write(Bit_SET);
} //deselect()



void SDCard::init_spi(SPI_TypeDef* _spi, uint8_t _cs) {


	sd_cs = _cs;
} //
