#include <stdio.h>
#include <string.h>

#include "bluepill.h"
#include "millis.h"
#include "Led.h"
#include "Serial.h"
#include "WS2813Strip.h"
#include "Spi.h"

#include "stm32f10x.h"

#include "ff.h"
#include "diskio.h"
//#include "my_sdcard.h"


#define PIXELS	64


#define FCLK_SLOW() { SPIx_CR1 = (SPIx_CR1 & ~0x38) | 0x28; }	/* Set SCLK = PCLK / 64 */
#define FCLK_FAST() { SPIx_CR1 = (SPIx_CR1 & ~0x38) | 0x00; }	/* Set SCLK = PCLK / 2 */


//extern volatile DSTATUS Stat;
extern volatile UINT Timer1, Timer2;


Led 		board_led(PC13);
Serial		serial(USART1);
//WS2813Strip	led_strip(PIXELS);

//uint32_t	colors[] = {0xffff00, 0xffff00, 0xffff00, 0xffff00, 0xffff00};
//uint32_t	colors[] = {0xff00ff, 0xff00ff, 0xff00ff, 0xff00ff, 0xff00ff};
//uint32_t colors[PIXELS];

Spi			sd(SPI1);
Pin			sd_cs(PA4, Bit_SET);

FATFS FatFs;				/* File system object for each logical drive */
BYTE Buff[4096] __attribute__ ((aligned (4))) ;	/* Working buffer */
FIL file_pointer;
BYTE res;
UINT cnt;

extern "C" void SysTick_Handler(void);
extern "C" void disk_timerproc(void);
//extern void disk_timerproc(void);

volatile UINT Timer;
/* Cortex-M3 System timer */
#define	SYST_CSR	(*(volatile uint32_t*)0xE000E010)
#define	SYST_RVR	(*(volatile uint32_t*)0xE000E014)
#define	SYST_CVR	(*(volatile uint32_t*)0xE000E018)
#define	SYST_CALIB	(*(volatile uint32_t*)0xE000E01C)


void main(void) {

	millis_init();
	board_led.turn_on();

	//disk_timerproc();

	/* Enable SysTick timer as 1 ms interval timer */
	SYST_RVR = 72000000 / 1000 - 1;
	SYST_CVR = 72000000 / 1000 - 1;
	SYST_CSR = 0x07;

	serial.send("\nstart.\n");
	sd_cs.digital_write(Bit_RESET);
	delay(100);

	f_mount(&FatFs, "", FA_READ);
	//f_mkdir("dir2");
	const TCHAR * file_name = "abc.txt";
	res = f_open(&file_pointer, file_name, FA_READ);
	if (res != FR_OK) while(1);
	cnt = 10;
	res = f_read(&file_pointer, Buff, cnt, &cnt);
	if (res != FR_OK) while(1);

	for (uint8_t i = 0; i < cnt; i++) {
		serial.send_char(Buff[i]);
	}
	serial.send("\n");

    while(1) {

    }

} //main


void SysTick_Handler (void) {

	WORD n = 0;
	BYTE s;

	Timer++;	/* Increment performance counter */

	if (n++ > 1000) {
		board_led.toggle();
		n = 0;
	}

	disk_timerproc();
}

DWORD get_fattime (void) {
	return 0;
}
